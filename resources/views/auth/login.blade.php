@extends('template')

@section('title', 'Přihlášení - Motivátor')

@section('content')

<h1>Přihlášení</h1>

{!! Form::open(array('action' => 'Auth\AuthController@postLogin')) !!}
  <dl class="login">
    <dt><label for="email">Email:</label></dt>
    <dd><input type="email" placeholder="jan.novak@email.cz" name="email" value="{{ old('email') }}"></dd>
    <dt><label for="password">Heslo:</label></dt>
    <dd><input type="password" name="password"></dd>
    <dt>Odeslání formuláře</dt>
    <dd><label>Zapamatovat si přihlášení <input type="checkbox" name="remember"></label></dd>
    <dd><input type="hidden" name="_token" value="{{ csrf_token() }}"><input type="submit" name="submit" value="Přihlásit se"></dd>
  </dl>
{!! Form::close() !!}

@endsection
