<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="cs">
  <head>
    <meta charset="utf-8"/>
    <meta content="initial-scale=1" name="viewport"/>
    <meta content="InternetGuru" name="author"/>
    <meta content="Motivátor - Aplikace pro Vaše lepší výkony" name="description"/>
    <meta content="cvičení, motivace, spolupráce, projekty" name="keywords"/>
    <title>@yield('title')</title>
    {!! HTML::style('css/default.css') !!}
    {!! HTML::style('css/motivator.css') !!}
  </head>

  <body>
    <div id="header">
      <div class="top">
        <p class="motto">{!! link_to('/', 'Motivátor') !!} &ndash; pro Vaše lepší výkony</p>
        <ul>
          @if(Auth::guest())
          <li><a href="{{ action('Auth\AuthController@getLogin') }}">Přihlásit se</a></li>
          <li><a href="{{ action('Auth\AuthController@getRegister') }}">Registrovat se</a></li>
          @else
          <li>Přihlášený uživatel: {{ Auth::user()->name }} {{ Auth::user()->surname }} <a href="{{ action('Auth\AuthController@getLogout') }}">Odhlásit se</a></li>
          @endif
        </ul>
      </div>
      @include('errors')
      @yield('header')
    </div>
    <div id="content">
      @yield('content')
    </div>
  </body>
</html>