@extends('template')

@section('title', 'Registrace - Motivátor')

@section('content')

<h1>Úprava plánu</h1>
{!! Form::open(array('action' => array('PageViewController@postEditPlan', $plan->plan_ID))) !!}
  <dl>
    <dt><label for="nazevplanu">Název plánu</label></dt>
    <dd><input id="nazevplanu" type="text" name="name" value="{{ $plan->name }}"></dd>
    <dt><label for="popisplanu">Popis plánu</label></dt>
    <dd><textarea id="popisplanu" name="description">{{ $plan->description }}</textarea></dd>
    <dt>Odeslat formulář</dt>
    <dd><input id="odeslat" type="submit" name="submit" value="Potvdit změny"></dd>
  </dl>
{!! Form::close() !!}

<h2>Aktivity</h2>
@foreach($activities as $activity)
@if($activity->active == 1)
{!! Form::open(array('action' => array('PageViewController@removeActivity', $plan->plan_ID, $activity->activities_plan_ID) )) !!}
  <dl class="seznam-aktivit-planu">
    <dt>{{ $activity->description }}</dt>
    <dd><label>Množství <input type="number" name="quantity" placeholder="0" min=0 value="{{ $activity->quantity }}"></label></dd>
    <dd><label>Jednotka <input type="text" name="unit" placeholder="hod" value="{{ $activity->activity->unit }}"></dd></label>
    <dd><input type="submit" name="submit" value="Odstranit"></dd>
  </dl>
{!! Form::close() !!}
@endif
@endforeach

<!--
<h2>Nová aktivita</h2>
<form id="novaaktvita" action="novaaktivit" method="post">
  <dl>
    <dt><label for="popisaktivity">Popis aktivity</label></dt>
    <dd><textarea id="popisaktivity" placeholder="Popis"></textarea></dd>
    <dt>Nová aktivita</dt>
    <dd><label>Jednotka <input id="jednotkaaktivity" type="text" name="jednotka" placeholder="hod"></dd></label>
    <dd><label>Množství <input id="mnozstviaktivity" type="number" name="mnozstvi" placeholder="0" min=0></dd></label>
    <dd><label>Kategorie <select>
      <option>Běhání</option>
      <option>Skákání</option>
      <option>Box</option>
      <option>Kondice</option>
    </select></label></dd>
    <dt>Odeslání formuláře</dt>
    <dd><input type="submit" name="submit" value="Vytvořit aktivitu"></dd>
</form>
-->


<h2>Přidat aktivitu</h2>
{!! Form::open(array('action' => array('PageViewController@addActivity', $plan->plan_ID) )) !!}
<dl class="seznam-aktivit-planu">
@foreach(\App\Models\Activity::all() as $activity)
  <dt><label><input type="checkbox" name="activityid[]" value="{{ $activity->activity_ID }}"> {{ $activity->activity }}</label></dt>
  <dd><label>Množství: <input type="number" name="quantity{{ $activity->activity_ID }}" placeholder="2" min=0> {{ $activity->unit }}</label></dd>
  <dd><label>Popisek: <input type="text" name="description{{ $activity->activity_ID }}" value="{{ $activity->activity }}"></label></dd>
@endforeach
  <dd><input type="submit" name="submit" value="Odeslat"></dd>
</dl>
{!! Form::close() !!}


@endsection