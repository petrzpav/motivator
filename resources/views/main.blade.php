@extends('template')

@section('title', 'Motivátor')

@section('content')

  <h1>Motivátor</h1>
  <h2>Žádosti o přátelsví</h2>
  <dl class="zadosti">
  @foreach(Auth::user()->pendingFriends as $f)
    <dt>{{ $f->friend->name }} ({{ $f->friend->email }})</dt>
    <dd><button class="accept" type="button">Přijmout</button><button class="deny" type="button">Zamítnout</button></dd>
  @endforeach
  </dl>

{{--   <h2>Váhy</h2>
  <ul>
  @foreach ($weights as $weight)
    <li>{{ $weight->date }}: {{ $weight->weight }} kg</li>
  @endforeach
  </ul>
 --}}

  <h2>Plány</h2>

  @foreach($plans as $plan)
    @if($plan->active == 1)
      <h3 class="nazev-planu">{{ $plan->name }}</h3>
      <ul class="uprava-planu">
        <li><a href="" title="Deaktivovat plán">Deaktivovat</a></li>
        <li>{!! link_to_action('PageViewController@getEditPlan', 'Upravit plán', ['id' => $plan->plan_ID]) !!}</li>
      </ul>
      <p class="popis-planu">{{ $plan->description }}</p>
      <h4>Poslední záznamy o aktivitách (staticky)</h4>
      <ul class="seznam-zaznamu">
      <li>Posilovna<dl>
          <dt>Břicho</dt><dd>1 hod</dd>
          <dt>Paže</dt><dd>0,5 hod</dd>
          <dt>Nohy</dt><dd>0,5 hod</dd>
      </dl></li>
      <li>Cvičení doma<dl>
          <dt>Břicho</dt><dd>1 hod</dd>
          <dt>Paže</dt><dd>0,5 hod</dd>
      </dl></li>
      <li>Cvičení v parku<dl>
          <dt>Běh</dt><dd>1 hod</dd>
          <dt>Nohy</dt><dd>0,5 hod</dd>
      </dl></li>
      </ul>
    @endif
  @endforeach

{{--   <h3>Neaktivní</h3>
  <ul>
  @foreach ($plans as $plan)
    @if($plan->active == 0)
      <li>{{ $plan->name }} ({{ $plan->description }})</li>
    @endif
  @endforeach
  </ul>
 --}}
  <h3>Přidat nový plán</h3>

  {!! Form::open(array('action' => 'PageViewController@newPlan')) !!}
  <dl>
    <dt>{!! Form::label('name', 'Jméno plánu') !!}</dt>
    <dd>{!! Form::text('name', @$name) !!}</dd>
    <dt>{!! Form::label('description', 'Popis') !!}</dt>
    <dd>{!! Form::textarea('description', @$description) !!}</dd>
    <dt>Odeslání formuláře</dt>
    <dd>{!! Form::submit('Přidat plán') !!}</dd>
  <ul>
  {!! Form::close() !!}
@endsection