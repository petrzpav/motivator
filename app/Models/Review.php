<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Review
 */
class Review extends Model
{

    public $timestamps = false;
    /**
     * @var string $table Jméno tabulky v databázi, ke které tento model patří.
     */
    protected $table = 'review';
    /**
     * @var string $primaryKey Jméno primárního klíče v tabulce.
     */
    protected $primaryKey = 'reviews_ID';

    /**
     * @var Array $fillable  Pole jmen atributů v databázi.
     */
    protected $fillable = [
        'reviews_ID',
        'date',
        'overall_rating',
        'comment',
        'training_ID',
        'user_ID',
        'created'
    ];

    protected $guarded = [];


    /**
     * @return User Metoda vrací uživatele, který hodnocení vytvořil.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    /**
     * @return Plan Metoda vrací plán, ke kterému se hodnocení vztahuje.
     */
    public function plan()
    {
        return $this->belongsTo('App\Models\Plan');
    }
        
}