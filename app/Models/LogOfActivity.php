<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LogOfActivity
 */
class LogOfActivity extends Model
{

    public $timestamps = false;
    /**
     * @var string $table Jméno tabulky v databázi, ke které tento model patří.
     */
    protected $primaryKey = 'log_ID';
    /**
     * @var string $primaryKey Jméno primárního klíče v tabulce.
     */
    protected $table = 'log_of_activity';

    /**
     * @var Array $fillable  Pole jmen atributů v databázi.
     */
    protected $fillable = [
        'activities_plan_ID',
        'log_ID',
        'quantity'
    ];

    protected $guarded = [];

    /**
     * Metoda přidá záznam o aktivitě k požadovaným aktivitám.
     * @param Log Přidávaný log.
     */
    public function addLog(Log $log)
    {
           
    }

        
}