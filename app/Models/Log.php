<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



/**
 * Class Log
 */
class Log extends Model
{

    public $timestamps = false;
    /**
     * @var string $table Jméno tabulky v databázi, ke které tento model patří.
     */
      protected $primaryKey = 'log_ID';
    /**
     * @var string $primaryKey Jméno primárního klíče v tabulce.
     */
    protected $fillable = [
    /**
     * @var Array $fillable  Pole jmen atributů v databázi.
     */
    protected $table = 'log';
        'log_ID',
        'date',
        'comment'
    ];

    protected $guarded = [];

    /**
     * @return Array(ActivitiesPlan) Metoda vrací všechny plány aktivit, ke kterým se log vztahuje.
     */
    public function activitiesPlan()
    {
    	return $this->hasManyThrough('App\Models\LogOfActivity', 'App\Models\ActivitiesPlan', 'activities_plan_ID', 'activities_plan_ID');
    }


}