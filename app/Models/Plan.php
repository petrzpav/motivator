<?php

namespace App\Models;

use Auth;
use App\Models\ValidationModel;

/**
 * Class Plan
 */
class Plan extends ValidationModel
{

    public $timestamps = false;
    /**
     * Jméno tabulky v databázi, ke které tento model patří.
     * @var string $table
     */
    protected $table = 'plan';

    /**
     * Jméno primárního klíče v tabulce.
     * @var string $primaryKey
     */
    protected $primaryKey = 'plan_ID';


    /**
     * Pole jmen atributů v databázi.
     * @var Array $fillable
     */
    protected $fillable = [
        'training_ID',
        'user_ID',
        'active',
        'name',
        'description'
    ];
    protected $guarded = [];
    /**
     * Funkce pro vlastní validaci.
     * @var null $customValidate
     */
    protected $customValidate = null;

    /**
     * Podmínky pro validaci.
     * @var Array $rules
     */
    protected static $rules = [
      "name" => "required"
    ];

    /**
     * Chybové hlášky při porušení pravidel v $rules.
     * @var Array $messages
     */
    protected static $messages = [
      "name.required" => "Jméno plánu je povinné"
    ];
    /**
     * Zde se vytváří funkce pro vlastní validaci.
     * @param array
     */
    public function __construct(array $attributes = array())
    {
      parent::__construct($attributes);
      //$this->buildCustomValidate();
    }


    /**
     * Metoda ověřuje, jestli se uživatel nepokouší vložit plán se stejným jménem, který už je v databázi.
     * @return void Metoda nevrací nic.
     */
    /*public function buildCustomValidate()
    {
      $this->customValidate = function($validator){
        if(!empty(\DB::select('select * from plan where name=? and user_ID=?', array($this->name, Auth::user()->user_ID))))
          $validator->errors()->add("name","Jméno plánu je již obsazené");
      };
    }*/

    /**
     * @return Array(Review) Metoda vrací seznam všech hodnocení k danému plánu.
     */
    public function review()
    {
        return $this->hasMany('App\Models\Review');
    }


    /**
     * @return User Metoda vrací uživatele, kterému patří cvičební plán.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }


    /**
     * @return Array(Activity) Metoda vrací seznam všech aktivit zařazených cvičebního plánu.
     */
    public function activity()
    {
        return $this->hasManyThrough('App\Models\Activity', 'App\Models\ActivitiesPlan', 'activity_ID', 'activity_ID');
    }
    /**
     * @return Array(ActivitiesPlan) Metoda vrací seznam všech plánů k aktivitám zařazených do cvičebního plánu.
     */
    public function activitiesPlan()
    {
        return $this->hasMany('App\Models\ActivitiesPlan');
    }


}