<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Friend
 */
class Friend extends Model
{

    public $timestamps = false;
    /**
     * @var string $table Jméno tabulky v databázi, ke které tento model patří.
     */

    protected $primaryKey = 'friendship_ID';
    /**
     * @var string $primaryKey Jméno primárního klíče v tabulce.
     */
    protected $table = 'friendship';

    /**
     * @var int $REQUEST_PENDING Konstanta, která označuje, že žádost nebyla vyřízena.
     */
    const REQUEST_PENDING = 0;
    /**
     * @var int $REQUEST_ACCEPTED Konstanta, která označuje, že žádost byla přijata.
     */
    const REQUEST_ACCEPTED = 1;
    /**
     * @var int $REQUEST_DENIED Konstanta, která označuje, že žádost nebyla přijata.
     */
    const REQUEST_DENIED = 2;

    /**
     * @var Array $fillable  Pole jmen atributů v databázi.
     */
    protected $fillable = [
    	'friendship_ID',
        'user1',
        'user2',
        'state',
        'date'
    ];

    protected $guarded = [];

    /**
     * @return User Vrací uživatele, který požádal o přátelství.
     */
    public function user() {
      return $this->hasOne('App\Models\User', 'user_ID');
    }
    /**
     * @return User Vrací uživatele, který byl požádán o přátelství.
     */
    public function friend() {
      return $this->belongsTo('App\Models\User', 'user2');
    }

    /**
     * Metoda změní stav žádosti z REQUEST_PENDING na REQUEST_ACCEPTED.
     * @return void Metoda nevrací nic.
     */
    public function acceptFriendRequest()
    {
    	# code...
    }
    /**
     * Metoda změní stav žádosti z REQUEST_PENDING na REQUEST_DENIED.
     * @return void Metoda nevrací nic.
     */
    public function denyFriendRequest()
    {
    	# code...
    }


}