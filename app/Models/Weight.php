<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Weight
 */
class Weight extends Model
{

    public $timestamps = false;
    /**
     * Jméno tabulky v databázi, ke které tento model patří.
     * @var string $table
     */
    protected $table = 'weight';
    /**
     * Jméno primárního klíče v tabulce.
     * @var string $primaryKey
     */
    protected $primaryKey = 'weight_ID';

    /**
     * Pole jmen atributů v databázi.
     * @var Array $fillable
     */
    protected $fillable = [
        'weight_ID',
        'user_ID',
        'date',
        'weight'
    ];

    protected $guarded = [];


    /**
     * @param  Date Neformátované datum
     * @return Date Metoda vrací datum ve správném formátu.
     */
    public function getDateAttribute($val) {
      $date = new \DateTime($val);
      return $date->format("j. m. Y");
    }


    /**
     * @return User Metoda vrací uživatele, ke kterému se váha vztahuje.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}