<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ActivitiesPlan
 */
class ActivitiesPlan extends ValidationModel
{

    public $timestamps = false;
     /**
     * Jméno tabulky v databázi, ke které tento model patří.
     * @var string $table
     */
    protected $table = 'activities_plan';
     /**
     * Jméno primárního klíče v tabulce.
     * @var string $primaryKey
     */
    protected $primaryKey = 'activities_plan_ID';

    /**
     * Pole jmen atributů v databázi.
     * @var Array $fillable
     */
    protected $fillable = [
        'activities_plan_ID',
        'plan_ID',
        'activity_ID',
        'quantity',
        'description',
        'day_of_change',
        'active',
        'day_of_week'
    ];

    protected $guarded = [];

    /**
     * Podmínky pro validaci.
     * @var Array $rules
     */
    protected static $rules = [
      "quantity" => "required"
    ];

    /**
     * Chybové hlášky při porušení pravidel v $rules.
     * @var Array $messages
     */
    protected static $messages = [
      "name.quantity" => "Počet úkonů je povinný"
    ];

    /**
     * Metoda zajišťující všechny záznamy o prováděné aktivitě.
     * @return Array(Log) Pole záznamů o provádění aktivity.
     */
    public function log()
    {
        return $this->hasManyThrough('App\Models\Log', 'App\Models\LogOfActivity', 'log_ID', 'log_ID');
    }
    /**
     * Metoda zajišťující záznamy o provádění aktivit za daný týden.
     * @param  int $week Požadovaný týden.
     * @return Array(Log)       Pole záznamů o provádění aktivity za daný týden.
     */
    public function getLogWeek($week)
    {

        return $this->log()->where('week', "=", "1");
    }
    /**
     * Vrací hodnotu, na kolik byla aktivita za daný týden splněna.
     * @param  int $week Požadovaný týden.
     * @return int       Plnění aktivity.
     */
    public function getProgressWeek($week)
    {
        # code...
        $progress = null;
        return $progress;
    }
    /**
     * Metoda obstarávající aktivitu ze seznamu aktivit daného plánu.
     * @return Activity Požadovaná aktivita.
     */
    public function activity()
    {
        return $this->belongsTo('App\Models\Activity', 'activity_ID');
    }
    /**
     * Metoda obstarávající hodnocení k dané aktivitě.
     * @return Array(ActivityReview) Pole hodnocení k aktivitě.
     */
    public function activityReview()
    {
        return $this->hasMany('App\Models\ActivityReviews');
    }


}