<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Activity
 */
class Activity extends Model
{

    public $timestamps = false;
    /**
     * Jméno tabulky v databázi, ke které tento model patří.
     * @var string $table
     */
    protected $table = 'activity';
    /**
     * Jméno primárního klíče v tabulce.
     * @var string $primaryKey
     */
    protected $primaryKey ='activity_ID';

    /**
     * Pole jmen atributů v databázi.
     * @var Array $fillable
     */
    protected $fillable = [
        'activity_ID',
        'category_ID',
        'activity',
        'description',
        'unit'
    ];

    protected $guarded = [];

    /**
     * Metoda zjistí kategorii, do které aktivita spadá.
     * @return Category Kategorie, do které aktivita patří.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Metoda zjišťující všechny plány, do kterých je zahrnuta aktivita.
     * @return Array(ActivitiesPlan) Pole plánů, do kterých aktivita patří.
     */
    public function activitiesPlan()
    {
        return $this->hasMany('App\Models\activitiesPlan');
    }




}