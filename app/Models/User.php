<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


/**
 * Class User
 */
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
  use Authenticatable, Authorizable, CanResetPassword;

	public $timestamps = false;
	/**
	 * Jméno tabulky v databázi, ke které tento model patří.
	 * @var string $table
	 */
	protected $table = 'user';
	/**
	 * Jméno primárního klíče v tabulce.
	 * @var string $primaryKey
	 */
	protected $primaryKey = 'user_ID';

	/**
	 * Pole jmen atributů v databázi.
	 * @var Array $fillable
	 */
	protected $fillable = [
		'name',
		'surname',
		'nickname',
		'email',
		'birthdate',
		'password'
	
	];


	protected $guarded = [];

	/**
	 * Pole atributu, ktere maji byt skryte pro serializaci.
	 * @var Array
	 */
 	protected $hidden = ['password', 'remember_token'];
 	


 	/**
 	 * @return Array(Weight) Metoda vrací seznam všech záznamů o vážení uživatele.
 	 */
	public function weight() {
	  return $this->hasMany('App\Models\Weight');

	}

	/**
	 * @return Array(Plan) Metoda vrací seznam všech cvičebních plánů uživatele.
	 */
	public function plan()
	{
		return $this->hasMany('App\Models\Plan');
	}

	/**
	 * @return Array(Friend) Metoda vrací seznam všech přátel uživatele.
	 */
	public function friend()
	{
		return $this->hasMany('App\Models\Friend', 'user1');
		
	}

	/**
	 * @return Array(Friend) Metoda vrací seznam žádostí o přátelství.
	 */
  	public function pendingFriends() {
    return $this->friend()->where("state", "=", Friend::REQUEST_PENDING);
   
  }

}