<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ActivityReview
 */
class ActivityReview extends Model
{

    public $timestamps = false;
     /**
     * Jméno tabulky v databázi, ke které tento model patří.
     * @var string $table
     */
    protected $table = 'activityreview';
    /**
     * Jméno primárního klíče v tabulce.
     * @var string $primaryKey
     */
    protected $primaryKey = 'activities_reviews_ID';

     /**
     * Pole jmen atributů v databázi.
     * @var Array $fillable
     */
    protected $fillable = [
        'activity_reviews_ID',
        'activities_plan_ID',
        'reviews_ID',
        'rating',
        'comment'
    ];

    protected $guarded = [];

        
}