<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 */
class Category extends Model
{

    public $timestamps = false;
    /**
     * @var string $table Jméno tabulky v databázi, ke které tento model patří.
     */
    protected $table = 'category';
    /**
     * @var string $primaryKey Jméno primárního klíče v tabulce.
     */
    protected $primaryKey = 'category_ID';

    /**
     * @var Array $fillable  Pole jmen atributů v databázi.
     */
    protected $fillable = [
        'category_ID',
        'category',
        'description'
    ];

    protected $guarded = [];

    /**
     * @return Array(Activity) Metoda vrací seznam všech aktivit, které patří do dané kategorie.
     */
    public function activity()
    {
    	return $this->hasMany('App\Models\Activity');
    }



        
}