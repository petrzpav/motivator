<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'auth'], function () {

  // All my routes that needs a logged in user
  Route::get('/', 'PageViewController@index');
  Route::get('/editplan/{id}', 'PageViewController@getEditPlan');
  Route::post('/editplan/{id}', 'PageViewController@postEditPlan');
  Route::post('/newplan', 'PageViewController@newPlan');
  Route::post('/addactivity/{planid}', 'PageViewController@addActivity');
  Route::post('/removeactivity/{planid}/{activityid}', 'PageViewController@removeActivity');

});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::controllers([
   'password' => 'Auth\PasswordController',
]);


